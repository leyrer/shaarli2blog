#!/usr/bin/perl 

# apt install libdatetime-format-strptime-perl libssl-dev libxml-feed-perl

use strict;
use warnings;

use XML::Feed;
use LWP::UserAgent;
use Data::Dumper;
use File::Spec;
use File::Path qw(make_path);
use DateTime;
use DateTime::Format::Strptime;
use Encode qw(decode encode);
use Getopt::Long;
# use utf8;

# You may change these values START

# URL to the Shaarli RSS Feed, optionally with searchtag, ...
# Add "&nb=all" as an URL parameter via the command line to get all entries
# in case you need to recreate postings
my $blogurl = "https://delicious.leyrer.priv.at/feed/atom";

# Folder in which to create directories and content files
my $destfolder = "/home/leyrer/blogtxt";

# files should start with this string (followed by date string and '.txt')
my $fileprefix = "shaarli-";

# headline string for one link entry
my $headlinestring = "h4";

# Headlines start with this string
my $subjectprefix = "Links from ";

# Turn debugging on/off
my $DEBUG = 1;

# You may change these values END

$|=1;

# Lets have a "nice" user agent string
my $ua = LWP::UserAgent->new (
	agent => "shaarli2blog"
);
$ua->ssl_opts( 
	verify_hostname => 1
);


# Sane defaults for command line parameters
my ($tsec,$tmin,$thour,$fday,$fmon,$fyear,$twday,$tyday,$tisdst) = localtime(yesterday());
$fyear += 1900; $fmon++;
my $tyear = $fyear; my $tmon = $fmon; my $tday = $fday;
my @only = ('publish'); # default value to emulate legacy behaviour
my @everythingbut = ();

# Get command line parameters
GetOptions ("fyear=i"	=> \$fyear,
			"fmonth=i"	=> \$fmon,
			"fday=i"	=> \$fday,
			"tyear=i"	=> \$tyear,
			"tmonth=i"	=> \$tmon,
			"tday=i"	=> \$tday,
			"blogurl=s"	=> \$blogurl,
			"fileprefix=s"	=> \$fileprefix,
			"subjectprefix=s"	=> \$subjectprefix,
			"only=s@"	=> \@only,
			"everythingbut=s@"	=> \@everythingbut,
		   )
or die("Error in command line arguments\n");
print "START: $fyear, $fmon, $fday, $tyear. $tmon, $tday\n" if($DEBUG);;

# Initialize stuff with values from command line parameters
my $dt_from = DateTime->new(
		year       => $fyear,
		month      => $fmon,
		day        => $fday,
		hour       => 5,
		minute     => 0,
		second     => 0,
		time_zone  => 'local',
);
my $dt_to = DateTime->new(
		year       => $tyear,
		month      => $tmon,
		day        => $tday,
		hour       => 5,
		minute     => 0,
		second     => 0,
		time_zone  => 'local',
);

my %incl;
foreach (@only) {
	$incl{$_} = 1;
}

my %excl;
foreach (@everythingbut) {
	$excl{$_} = 1;
}

# Lets start the actual work ...
print "Fetching " . $blogurl . " ...\n" if($DEBUG);
my $response = $ua->get($blogurl);
die "Error at " . $blogurl . "\n" . $response->status_line . "\n Aborting." unless $response->is_success;

my $raw_content = $response->decoded_content;
my $feed = XML::Feed->parse(\$raw_content);

my $entries;
my $categories;

foreach my $item ($feed->entries) {
	#print Dumper($item) if($item->title =~ /spell/i);
	#print Dumper($item->title) if($item->title =~ /spell/i);
	# print "SKIP: " . skipEntry($item) . "\n" if($item->title =~ /spell/i);
	next if skipEntry($item);
	# Build hash with ready-built strings for each day
	$entries->{substr($item->issued, 0, 10)} .= entry2string($item);
	# Build hash with categories for each day
	$categories->{substr($item->issued, 0, 10)} .= entry2categorystring($item);
}

# What to generate ...
my $working = $dt_from->clone();
print "checking date range for URLs ...\n" if ($DEBUG);
do {
	my $filedate = sprintf( "%4.4d-%2.2d-%2.2d", $working->year, $working->month, $working->day);
	if( exists $entries->{$filedate} ) {
		my $dt_touch = $working->epoch() + 60 * 60 * 24; # timestamp for next day
		print "Creating file for day: $filedate, $dt_touch\n" if($DEBUG);
		&writeFile($filedate, $entries->{$filedate}, $dt_touch, $categories->{$filedate});
	} else {
		print "No entry found, skipping $filedate ...\n" if($DEBUG);
	}
	$working->add( days => 1 );
} until ( $working->epoch() >= $dt_to->epoch() );

print "DONE !";
exit;


sub skipEntry {
	my ($item) = @_;
	my $ret = 1;
	my $cat = '';
	my $inclcount = 0;

	foreach $cat ($item->category) {
		if($excl{$cat}) {
			$inclcount = -1;
			$ret = 1;
			last;
		} elsif ($incl{$cat}) {
			$inclcount++;
		}
	}
	# Publish only items that are tagged with all hashstags
	if( $inclcount == (keys %incl)) {
		$ret = 0;
	}
	return($ret);
}

sub yesterday { 
	# Borrowed from perlfaq4; note changes below.
	my $now  = defined $_[0] ? $_[0] : time;
	my $then = $now - 60 * 60 * 24;
	my $ndst = (localtime $now)[8] > 0;
	my $tdst = (localtime $then)[8] > 0;
	
	# Added '=' to avoid warning (and return)
	$then -= ($tdst - $ndst) * 60 * 60;
	return($then);
}

sub writeFile {
	my ($fdate, $content, $timestamp, $tcategories) = @_;
	my @folders;
	push(@folders, $destfolder);
	push(@folders, "y" . substr($fdate, 0, 4));
	push(@folders, "m" . substr($fdate, 5, 2));

	my $fn = File::Spec->catfile( @folders, $fileprefix . $fdate . ".txt");
	print "Working on file $fn ...\n" if ($DEBUG);
	#print "BODY: $content\n" if ($DEBUG);
	print Dumper($content) if( $DEBUG);

	# Add categories from sharli to blog post
	my $p_categories = entryCats2String($tcategories);

	my $d = File::Spec->catdir(@folders);
	if(not -d $d) {
		make_path($d) or die "Error creating folder $d.\n$!\n";
	}

	open(TXT, ">$fn") or die "Write error for file '$fn'. $!\n";
	# binmode(TXT, ":utf8");
	print TXT << "__UND_AUS__";
$subjectprefix$fdate
meta-Author: <a rel="author" href="/static/about-me.html">Martin Leyrer</a>
Tags: links, delicious, shaarli, collection$p_categories

__UND_AUS__
	print TXT "$content\n";
	close(TXT);
	
	# Properly timestamp the file
	print "Timestamping file $fn for $timestamp ...\n" if ($DEBUG);
	my $utimeerg= utime ( $timestamp, $timestamp, $fn);
}

sub entryCats2String {
	my ($cats) = @_;
	my %helper;
	my $p_categories = '';
	$cats = "" if(not defined $cats);

    @helper{ split /,/,  $cats } = ();
	foreach ( keys %helper ) {
		next if( $_ eq '');
		$p_categories .= ", $_";
	}
	return($p_categories);
}

sub entry2string {
	my ($entry) = @_;
	#print Dumper($entry);
	my $ret = '';
	my $e = &cleanupString($entry->title);
	my $c = $entry->content;
	my $body = $c->body;
	$body = encode("utf8", $body);
	$body =~ s/(<br>\&#8212; <a href=\"https:\/\/delicious\.leyrer.*)$//m;
	# cleanup body for my blog template
	$body =~ s/(<br\s*\/>\s)+/<br\/>\n/mgi;
	chomp $body;
	$ret .= "<$headlinestring><a href=\"" . $entry->link . '" title="' . $e . '">' . $e . "</a></$headlinestring>\n";
	$ret .= '<p>' . $body . "</p>\n\n";
	return($ret);
}

sub entry2categorystring{
	my ($entry) = @_;
	# print Dumper($entry);
	my $ret = '';
	my @c = $entry->category;
	my %temp;
	foreach (@c) {
		next if /^publish$/;
		$temp{$_} = 1;
	}
	foreach (keys %temp) {
		my $t = encode("utf8", $_);
		$ret .= ",$t";
	}
	return($ret);
}

sub cleanupString {
	my ($text) = @_;
	my $ret = $text;
	$ret =~ s/\"/\&quot;/gi;
	return($ret);
}

